import Main from "./component/main/Main";
import { withRouter } from "react-router-dom";

const App = withRouter(Main);
export default App;
