import React, { Component } from "react";
import Cookies from "universal-cookie";
import User from "../user/User";
import Admin from "../admin/Admin";
import {
  cook,
  header,
  api,
  errors,
  cooking,
  USER,
  ADMIN
} from "../../constants/constant";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {

    if (window.location.href.toString().includes("sign") || window.location.href.toString().includes("log")){
      return null
    }
    return (
      <React.Fragment>
        {this.props.parentProps.role === USER && <User {...this.props} />}
        {this.props.parentProps.role === ADMIN && <Admin {...this.props} />}

      </React.Fragment>
    );
  }
}

export default Home;
