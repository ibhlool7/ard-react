import "./Admin.css";
import "./sb-admin-2.css";
import Button from '@material-ui/core/Button';
import {
    AddSuperUserComponent,
    AddUserInvoiceComponent,
    PrintComp,
    ValueManagement,
    AddUserComponent,
    UserListComponent, api, HALF_YEARS, COLLEGE, cook
} from "../../constants/constant";
import AddUser from "./component/addUser/AddUser";
import UserList from "./component/listUser/ListUser";
import React, {Component} from "react";
import AddSuperUser from "./component/addSuperUser/AddSuperUser";
import ValueManagmer from "./component/valueManager/ValueManager"
import PrintComponent from './component/print/PrintComp'
import {withStyles} from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';

import ListAltIcon from '@material-ui/icons/ListAlt';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import PrintIcon from '@material-ui/icons/Print';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import {Paper} from '@material-ui/core'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import CssBaseline from "@material-ui/core/CssBaseline";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/Inbox';
import Avatar from '@material-ui/core/Avatar';
import Axios from "axios";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Cookies from "universal-cookie";
import Redirect from "react-router-dom/es/Redirect";



const categories = [
    {
        id: 'مدیریت کاربران',
        children: [
            {id: 'لیست کاربران', icon: <ListAltIcon/>},
            {id: 'کاربر جدید', icon: <PersonAddIcon/>},
            {id: 'پرینت', icon: <PrintIcon/>}
        ],
    },
    {
        id: 'مدیریت مقادیر',
        children: [
            {id: 'مقادیر', icon: <EqualizerIcon/>, active: true},
        ],
    },
];

const styles = (theme) => ({
    root: {
        height: "100vh",
        color: "white",
        textAlign: "auto",
        alignContent: "center",
        justifyContent: "center",
    },
    compView: {
        width: "100%",
        direction: 'rtl',
        height: "100%",
        overflowX: "hidden",
        zIndex: 1,
    },
    rootList: {
        backgroundColor: theme.palette.primary.main,
        width: "100%",
        direction: 'rtl',
        height: "100%",
        overflowX: "hidden",
        zIndex: 3,
    },

    item: {
        paddingTop: 10,
        paddingBottom: 10,

        textAlign: 'right',
        '&:hover,&:focus': {
            backgroundColor: 'rgba(255, 255, 255, 0.08)',
        },
    },
    large: {
        width: theme.spacing(20),
        height: theme.spacing(20),
        margin: "auto",
        alignItems: "center",
        justifyContent: "center",
    },
    avatarHolder: {
        margin: "auto",
        textAlign: "center",
        padding: theme.spacing(10)
    },
    icon: {
        color: "white"
    },
    active: {
        backgroundColor: theme.palette.primary.dark,
        textAlign: 'right',

    },
    'MuiTypography-body1': {
        color: "white"
    },
    anchor: {
        color: "white"
    }

});

function ListItemLink(props) {
    return <ListItem button component="a" {...props} />;
}

class Admin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comp: UserListComponent,
            out: false
        };
    }

    clickEvent = (event, param) => {
        event.preventDefault();
        this.setState({comp: param});
    };

    componentDidMount() {
        Axios.get(`${api}api/init/`)
            .then(res => {
                this.props.parentProps.dynamicInit(res.data)
            })
    }


    render() {
        const {classes, ...other} = this.props;
        let toRender = null;
        switch (this.state.comp) {
            case UserListComponent: {
                toRender = <UserList {...this.props} />;
                break;
            }
            case AddUserComponent: {
                toRender = <AddUser {...this.props} />;
                break;
            }

            case AddSuperUserComponent: {
                toRender = <AddSuperUser {...this.props} />;
                break;
            }

            case ValueManagement: {
                toRender = <ValueManagmer {...this.props}/>
                break;
            }
            case PrintComp: {
                toRender = <PrintComponent {...this.props}/>
                break;
            }
            default:
                toRender = <AddUser {...this.props} />;
        }

        return (
            <Grid container component="main" className={classes.root}>
                <CssBaseline/>
                <Grid item xs={10} sm={10} md={10} className={classes.compView}>{toRender}</Grid>
                <Grid item xs={2} sm={2} md={2} className={classes.rootList}>


                    <div className={classes.avatarHolder}>
                        {/*<Avatar src="/arm.png"  className={classes.large}/>*/}
                        <a href={"/"} className={classes.anchor}>
                            <img width={90} height={120} src={"/arm.png"}/>
                            <Typography className={classes.anchor} variant={"subtitle1"}>صفحه اصلی</Typography>
                        </a>

                    </div>

                    <List component="nav" aria-label="main mailbox folders">
                        <ListItem button

                                  className={this.state.comp === UserListComponent ? (classes.item, classes.active) : classes.item}
                                  onClick={e => {
                                      this.clickEvent(e, UserListComponent)
                                  }}>
                            <ListItemIcon>
                                <ListAltIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText className={"item-white"}
                                          primary={<Typography type="body2" style={{color: '#FFFFFF'}}>لیست
                                              کاربران</Typography>}/>
                        </ListItem>
                        <ListItem button
                                  className={this.state.comp === AddUserComponent ? (classes.item, classes.active) : classes.item}
                                  onClick={e => {
                                      this.clickEvent(e, AddUserComponent)
                                  }}>
                            <ListItemIcon>
                                <PersonAddIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText
                                primary={<Typography type="body2" style={{color: '#FFFFFF'}}>کاربر جدید</Typography>}/>
                        </ListItem>
                        <ListItem button
                                  className={this.state.comp === PrintComp ? (classes.item, classes.active) : classes.item}
                                  onClick={e => {
                                      this.clickEvent(e, PrintComp)
                                  }}>
                            <ListItemIcon>
                                <PrintIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText
                                primary={<Typography type="body2" style={{color: '#FFFFFF'}}>پرینت</Typography>}/>
                        </ListItem>
                    </List>
                    <Divider/>
                    <List component="nav" aria-label="secondary mailbox folders">
                        <ListItem button
                                  className={this.state.comp === ValueManagement ? (classes.item, classes.active) : classes.item}
                                  onClick={e => {
                                      this.clickEvent(e, ValueManagement)
                                  }}>
                            <ListItemIcon>
                                <EqualizerIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText
                                primary={<Typography type="body2" style={{color: '#FFFFFF'}}>مقادیر</Typography>}/>
                        </ListItem>
                    </List>
                    <Divider/>
                    <List component="nav" aria-label="secondary mailbox folders">
                        <ListItem button
                                  className={ classes.item}
                                  onClick={e => {
                                      // log out process
                                      const cookies = new Cookies();
                                      cookies.remove(cook);
                                      this.setState({
                                          out: true
                                      })
                                  }}>
                            <ListItemIcon>
                                <ExitToAppIcon className={classes.icon}/>
                            </ListItemIcon>
                            <ListItemText
                                primary={<Typography type="body2" style={{color: '#FFFFFF'}}>خروج</Typography>}/>
                        </ListItem>
                    </List>
                    {this.state.out && <Redirect to={"/sign-in"}/>}
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles, {withTheme: true})(Admin);
