import React, {useState, useEffect} from "react";
import './PrintComp.css'
import LinearProgress from '@material-ui/core/LinearProgress';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import * as constant from './../../../../constants/constant'
import Axios from "axios";
import TableComp from './TableComp'
import Grid from '@material-ui/core/Grid';
import {api, header, reportDomainPath} from "./../../../../constants/constant";


const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: 10,
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: 10,
    },
    root: {
        width: '10%',
        margin: "auto"
    },
    formPart: {
        width: "30%",
        margin: "auto",
        display: "grid",
    },
    printBtn: {
        marginLeft: "10px",
        marginBottom: "10px",
        marginTop: "10px",
        textAlign: "left"
    },
    text: {
        textAlign: "center"
    },
    container: {
        textAlign: "left"
    }
}));

export default function PrintComponent(props) {

    const [loading, setLoading] = useState(false)
    const [term, setTerm] = useState(constant.Terms[0])
    const [field, setField] = useState(constant.FieldsNames[0].value)
    const [data, setData] = useState(null)
    const [valueManager, setValueManager] = useState(null)


    const getValueFromValueManager = () => {
        Axios.get(`${api + constant.reportDomainPath}val/`, header(props.parentProps.auth)).then(res => {
            if (res.status === 204 || !res.data)
                return
            else {
                setValueManager(res.data);
            }
        }).catch(er => {
        })
    }
    useEffect(() => {
        getValueFromValueManager()
    }, [])

    const classes = useStyles();

    function getData(e) {
        e.preventDefault()
        setLoading(true)
        const path = constant.api + constant.reportDomainPath + `salaries/?` + `page=1&` + `college=${field}&` + `term=${term}&` + `size=${100}`
        Axios.get(path, constant.header(props.parentProps.auth)).then((res) => {
            setLoading(false)
            setData(res.data)
            console.log(res.data)
        }).catch(err => {
            console.log(err)
        })
    }

    function createTable() {
        return (<React.Fragment>
                <TableComp data={data} term={term} field={field} valueManager={valueManager} {...props}/>
                <Button className={classes.printBtn} variant="contained" color="primary" onClick={printTable}>
                    پرینت
                </Button>
            </React.Fragment>
        )
    }

    function printTable() {
        let prtContent = document.getElementById('salaryTable')
        let WinPrint = window.open()
        WinPrint.document.write(prtContent.innerHTML);
        copyStyles(document, WinPrint.document);
        WinPrint.document.close();
        WinPrint.focus();
        // WinPrint.print();
        // WinPrint.close();
    }

    function copyStyles(sourceDoc, targetDoc) {
        Array.from(sourceDoc.styleSheets).forEach(styleSheet => {
            if (styleSheet.cssRules) { // for <style> elements
                const newStyleEl = sourceDoc.createElement('style');

                Array.from(styleSheet.cssRules).forEach(cssRule => {
                    // write the text of each rule into the body of the style element
                    newStyleEl.appendChild(sourceDoc.createTextNode(cssRule.cssText));
                });
                targetDoc.head.appendChild(newStyleEl);
            } else if (styleSheet.href) { // for <link> elements loading CSS from a URL
                const newLinkEl = sourceDoc.createElement('link');

                newLinkEl.rel = 'stylesheet';
                newLinkEl.href = styleSheet.href;
                targetDoc.head.appendChild(newLinkEl);
            }
        });
    }

    return (
        <React.Fragment>
            {loading && <LinearProgress/>}
            <Grid item xs={12} sm={12} className={classes.formPart}>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="age-native-simple">دانشکده</InputLabel>
                    <Select
                        native
                        value={field}
                        onChange={(e) => {
                            setField(e.target.value)
                        }}>
                        {props.parentProps.college.map((item) =>
                            (<option value={item.value}>{item.name}</option>)
                        )}
                    </Select>
                </FormControl>

                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="age-native-simple">ترم</InputLabel>
                    <Select
                        native
                        value={term}
                        onChange={(e) => {
                            setTerm(e.target.value)
                        }}
                    >
                        {props.parentProps.semester.map((item) =>
                            (<option value={item}>{item}</option>)
                        )}
                    </Select>
                </FormControl>


                <Button variant="contained" color="primary" onClick={getData}>
                    نمایش
                </Button>

            </Grid>
            <hr/>
            <Paper id="tableParent" className={classes.container}>
                {data && createTable(data)}
                {!data && <p className={classes.text}>داده ای وجود ندارد</p>}
            </Paper>

        </React.Fragment>)
}