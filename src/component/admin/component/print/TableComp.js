import React, {useState, useEffect} from 'react'
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import {makeStyles} from "@material-ui/core/styles";
import ReactDom from 'react-dom'
import './PrintComp.css'
import Typography from "@material-ui/core/Typography";
import Container from '@material-ui/core/Container';
import * as constant from './../../../../constants/constant'
import Axios from "axios";
import {api, header} from "./../../../../constants/constant";
import Box from '@material-ui/core/Box';


const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: 10,
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: 10,
    },
    root: {
        width: '100%',
        margin: "auto",
        textAlign: "center"
    },
    headerAlign: {
        textAlign: 'center',
        marginTop: theme.spacing(5),
        marginBottom: theme.spacing(5)
    }, cellAlign: {
        textAlign: "center",
    }


}));

export default function TableComp(props) {
    const data = props.data;
    const classes = useStyles();

    const calculatePure = () => {
        let value = 0;
        data.forEach(item => {
            value += item.pure_salary;
        })
        return value

    }
    const calculateTax = () => {
        let value = 0;
        data.forEach(item => {
            value += item.tax;
        })
        return value

    }
    const calculateGather = () => {
        let value = 0;
        data.forEach(item => {
            value += item.gather_cost;
        })
        return value

    }
    const fieldInference = () => {
        let res = "";
        constant.FieldsNames.map((x) => {
            if (x.value === props.field) {
                res = x.name;
            }
        })
        return res;
    }
    const termInference = () => {
        let splited = props.term.split("-");
        let grater = splited[0];
        let lower = splited[1];
        let pivot = splited[2];
        let statement;
        switch (pivot) {
            case "1" : {
                statement = `فهرست حق التدریس دانشکده ` + fieldInference() + " ترم اول " + grater.toString() + "-" + lower.toString();
                break;
            }
            case "2" : {
                statement = `فهرست حق التدریس دانشکده ` + fieldInference() + " ترم دوم " + grater.toString() + "-" + lower.toString();
                break;
            }
            case "3" : {
                statement = `فهرست حق التدریس دانشکده ` + fieldInference() + " تابستان " + grater.toString() + "-" + lower.toString();
                break;
            }
        }
        return statement;
    }
    console.log(props.field)
    return (
        <div id="salaryTable">
            <Container className={classes.headerAlign}>
                <Typography variant={"h4"}>دانشگاه اردکان</Typography>
                <Typography variant={"h6"}>{termInference()}</Typography>
            </Container>

            <table className="table col-table">
                <thead className={"col-table-header"}>
                <tr>
                    <th className={"col-table"}>ردیف</th>
                    <th className={"col-table"}>نام و نام خانوادگی</th>
                    <th className={"col-table"}>جمع ساعات حق التدریسی</th>
                    <th className={"col-table"}>جمع ساعات داوری</th>
                    <th className={"col-table"}>جمع ساعات ناظر</th>
                    <th className={"col-table"}>جمع ساعات</th>
                    <th className={"col-table"}>نرخ هر ساعت</th>
                    <th className={"col-table"}>جمع هزینه</th>
                    <th className={"col-table"}>مالیات</th>
                    <th className={"col-table"}>خالص پرداختی</th>
                    <th className={"col-table"}>حقوق پایه</th>
                    <th className={"col-table"}>مخصوص</th>
                </tr>
                </thead>
                <tbody>

                {data.map((item, index) => {
                    return (
                        <tr key={index}>
                            <th className={"col-table"}>
                                {index + 1}
                            </th>
                            <td className={"col-table"}>
                                {item.first_name + " " + item.last_name}
                            </td>
                            <td className={"col-table"}>
                                {item.tech_time}
                            </td>
                            <td className={"col-table"}>
                                {item.judge_time}
                            </td>
                            <td className={"col-table"}>
                                {item.observing_time}
                            </td>
                            <td className={"col-table"}>
                                {item.sum_of_time}
                            </td>
                            <td className={"col-table"}>
                                {item.cost_of_time}
                            </td>
                            <td className={"col-table"}>
                                {item.gather_cost}
                            </td>
                            <td className={"col-table"}>
                                {item.tax}
                            </td>
                            <td className={"col-table"}>
                                {item.pure_salary}
                            </td>
                            <td className={"col-table"}>
                                {item.base_salary}
                            </td>
                            <td className={"col-table"}>
                                {item.special}
                            </td>
                        </tr>
                    )
                })}
                <tr>
                    <th className={"col-table"} colSpan={6}>
                        جمع کل (ریال)
                    </th>
                    <th className={"col-table"} rowSpan={1}>
                        {calculateGather()}
                    </th>
                    <th className={"col-table"}>{calculateTax()}</th>
                    <th className={"col-table"}>{calculatePure()}</th>
                </tr>


                </tbody>
            </table>
            <Box display="flex" justifyContent="center" m={12} p={12}>
                <Box p={7} m={5}>
                    <p className={classes.root}>
                        {props.valueManager.boss_name}
                        <br/>
                        رییس دانشکده </p>
                </Box>
                <Box p={7} m={5}>
                    <p className={classes.root}>
                        {props.valueManager.research_supervisor}
                        <br/>
                        معاون آموزشی پژوهشی
                    </p>
                </Box>
                <Box p={7} m={5}>

                    <p className={classes.root}>
                        {props.valueManager.teach_supervisor}
                        <br/>
                        مدیر آموزش و تحصیلات تکمیلی
                    </p>
                </Box>
                {props.parentProps.college.filter((item) => {
                    return item.value === props.field
                })[0].name === "مجازی" && <Box p={7} m={5}>
                    <p className={classes.root}>
                        {props.valueManager.virtual_supervisor}
                        <br/>
                        رییس آموزش های مجازی </p>
                </Box> || <Box p={7} m={5}>
                    <p className={classes.root}>
                        {props.valueManager.financial_supervisor}
                        <br/>
                        مدیر اداری مالی</p>
                </Box>}

            </Box>

        </div>)
}
