import React, {Component} from 'react'
import {FormGroup} from '@material-ui/core';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import "./ValueManager.css"
import NumberFormat from 'react-number-format';
import Axios from "axios";
import {
    cook,
    header,
    api,
    errors,
    cooking,
    USER,
    getPureValueFromFormatNumberComponent,
    ADMIN, userDomainPath, reportDomainPath
} from "../../../../constants/constant";
import Snackbar from "@material-ui/core/Snackbar";
import Backdrop from '@material-ui/core/Backdrop';
import Alert from "@material-ui/lab/Alert";
import InlineAlert from '../../../alerts/InlineAlert';
import Paper from '@material-ui/core/Paper';
import {withStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import * as constant from "../../../../constants/constant";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import List from "@material-ui/core/List";
import IconButton from "@material-ui/core/IconButton";
import ListSubheader from "@material-ui/core/ListSubheader";
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import Modal from "@material-ui/core/Modal";
import Fade from "@material-ui/core/Fade";


const styles = (theme) => ({
    form: {
        width: "100%",
        justifyContent: "center",
        textAlign: "center",
        padding: "15px"
    },
    fields: {
        width: "70%",
        direction: "rtl",
        margin: "10px"
    },
    paper: {
        width: "80%",
        marginRight: "auto",
        marginLeft: "auto",
        marginTop: "10px",
        padding: "10px",
        justifyContent: "center",
        textAlign: "center",
        marginBottom: "10px"
    },
    root: {
        margin: "auto"
    },
    bottom: {
        width: "10%",
        direction: "rtl",
        margin: "10px"
    },
    selectBox: {
        margin: "10px"
    },

    listSection: {
        backgroundColor: 'inherit'
    },
    ul: {
        backgroundColor: 'inherit',
        padding: 0,
    },
    lRoot: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
        position: 'relative',
        overflow: 'auto',
        // maxHeight: 300,
        // minHeight : 300,
        margin: "10px"
    },
    gRoot: {
        flexGrow: 1
    },
    control: {
        padding: theme.spacing(2),
    },
    deleteBtn: {
        color: theme.palette.primary.dark,
        '&:hover': {
            color: theme.palette.primary.light,
        }

    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper2: {
        verticalAlign: "center",
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        width: 350,
        padding: theme.spacing(2, 4, 3),
    },
    root3: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 200,
        },
    },
    semesterInput: {
        width: 50,
        textAlign: "center",
        textJustify: "center",
        bodyAlign: "center"
    },
    topMargin: {
        marginTop: 17,
        marginBottom: 3

    },
    vertical: {
        alignItems: "center"
    }
});


class ValueManager extends Component {

    constructor(props) {
        super(props);
        this.state = {
            boss_name: "",
            financial_supervisor: "",
            teach_supervisor: "",
            virtual_supervisor: "",
            research_supervisor: "",
            divided_to: 0,
            semesterModal: false,
            collegeModal: false,
            semesterValue1: undefined,
            semesterValue2: undefined,
            semesterValue3: undefined,
            collegeValue1: undefined,
            collegeValue2: undefined,
        }
    }

    generate = (element) => {
        return [0, 1, 2].map((value) =>
            React.cloneElement(element, {
                key: value,
            }),
        );
    }
    postData = (event) => {
        event.preventDefault()
        const data = {
            boss_name: this.state.boss_name,
            teach_supervisor: this.state.teach_supervisor,
            virtual_supervisor: this.state.virtual_supervisor,
            research_supervisor: this.state.research_supervisor,
            divided_to: this.state.divided_to,
            financial_supervisor: this.state.financial_supervisor
        }
        console.log(data)
        Axios.post(`${api + reportDomainPath}val/`, data, header(this.props.parentProps.auth)).then((res) => {
            this.setState({
                error: null,
                open: true
            })
        }).catch(er => {
            this.setState({error: er.response.data})
        })
    }

    getData = () => {
        Axios.get(`${api + reportDomainPath}val/`, header(this.props.parentProps.auth)).then(res => {
            if (res.status === 204 || !res.data)
                return
            else {
                console.log(res.data)
                this.setState({
                    boss_name: res.data.boss_name,
                    teach_supervisor: res.data.teach_supervisor,
                    virtual_supervisor: res.data.virtual_supervisor,
                    research_supervisor: res.data.research_supervisor,
                    divided_to: res.data.divided_to,
                    financial_supervisor: res.data.financial_supervisor
                })
            }
        }).catch(er => {
        })
    }

    commitDynamicData = () => {
        let data = {
            college: this.props.parentProps.college,
            semester: this.props.parentProps.semester
        }
        Axios.post(`${api}api/init/`, data)
            .then(res => {
                this.setState({
                    open: true
                })
            })
    }

    deleteSemesterHandler = event => {
        let array = [...this.props.parentProps.semester]
        let index = array.indexOf(event.target.id)
        if (index !== -1) {
            array.splice(index, 1);
            this.props.parentProps.setSemester(array)
        }
    }

    deleteCollegeHandler = event => {
        let array = [...this.props.parentProps.college]
        console.log(event.target.id)
        array.splice(event.target.id, 1)
        this.props.parentProps.setCollege(array)

    }

    guessCollegeDigestName = () => {
        let randList = ["a", "y", "t", "c", "x", "z", "p", "o", "u"];
        let currentCollege = []
        this.props.parentProps.college.map((item) => {
            currentCollege.push(item.value)
        })
        let cleanList = []
        randList.map((item) => {
            console.log(currentCollege.indexOf(item))
            if (currentCollege.indexOf(item) === -1) {
                cleanList.push(item)
            }
        })
        let num2 = cleanList.length - 1;
        console.log(cleanList)
        return cleanList[Math.floor(Math.random() * (num2 + 1))];
    }

    setValueHandler = (event) => {
        this.setState({
            [event.target.id]: event.target.value
        })
    }

    componentDidMount() {
        this.getData()
    }

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <Grid>
                    <Paper className={classes.paper}>


                        <form onSubmit={this.postData} className={classes.form} noValidate autoComplete="off"
                              ref="form">
                            <Grid item xs={12} sm={12}>
                                <Typography variant={"h6"}>مدیریت نام های موجود در فاکتور</Typography>
                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <TextField required={true} id="national_code" className={classes.fields}
                                           label="رییس دانشکده"
                                           value={this.state.boss_name}
                                           name={"boss_name"}
                                           onChange={change => {
                                               console.log(this.state.boss_name)
                                               this.setState({

                                                   boss_name: change.target.value
                                               })
                                           }}/>
                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <TextField required={true} id="national_code" className={classes.fields}
                                           label="معاون اداری مالی"
                                           value={this.state.financial_supervisor}
                                           name={"financial_supervisor"}
                                           onChange={change => {
                                               this.setState({
                                                   financial_supervisor: change.target.value
                                               })
                                           }}/>
                            </Grid>

                            <Grid item xs={12} sm={12}>
                                <TextField required={true} id="first_name" className={classes.fields}
                                           label="مدیر آموزش و تحصیلات تکمیلی"
                                           name={"teach_supervisor"}
                                           value={this.state.teach_supervisor}
                                           onChange={change => {
                                               this.setState({
                                                   teach_supervisor: change.target.value
                                               })
                                           }}
                                />

                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <TextField required={true} id="last_name" className={classes.fields}
                                           label="رییس آموزش های مجازی"
                                           name={"virtual_supervisor"}
                                           value={this.state.virtual_supervisor}
                                           onChange={change => {
                                               this.setState({
                                                   virtual_supervisor: change.target.value
                                               })
                                           }}/>
                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <TextField required={true} id="shaba" className={classes.fields}
                                           label="معاون آموزشی پژوهشی"
                                           name={"research_supervisor"}
                                           value={this.state.research_supervisor}
                                           onChange={change => {
                                               this.setState({
                                                   research_supervisor: change.target.value
                                               })
                                           }}/>

                            </Grid> <Grid item xs={12} sm={12}>
                            <TextField required={true} id="shaba" className={classes.fields} label="قابل تقسیم"
                                       name={"divided_to"}
                                       value={this.state.divided_to}
                                       onChange={change => {
                                           this.setState({
                                               divided_to: change.target.value
                                           })
                                       }}/>

                        </Grid>
                            <Button type="submit" variant="contained" className={classes.bottom} color="primary">
                                ثبت
                            </Button>
                        </form>

                        {this.state.open && (
                            <Snackbar
                                onClose={() => {
                                    this.getData()
                                    this.setState({
                                        open: false
                                    });
                                }}
                                open={this.state.open}
                                autoHideDuration={5000}
                            >
                                <Alert severity="success">ثبت گردید</Alert>
                            </Snackbar>
                        )}
                        {
                            this.state.error && <InlineAlert error={this.state.error}/>
                        }
                    </Paper>
                </Grid>
                <Grid>
                    <Paper className={classes.paper}>
                        <Grid item xs={12} sm={12}>
                            <Typography variant={"h6"}>مدیریت مقادیر پویا</Typography>
                        </Grid>
                        <Grid container spacing={3}>
                            <Grid item xs={12} sm={6}>
                                <List className={classes.lRoot} subheader={<li/>}>
                                    <ListSubheader>{`دانشکده ها`}</ListSubheader>
                                    {this.props.parentProps.college.map((coll, index) => (
                                        <li key={`section-${coll.name}`} id={index} className={classes.listSection}>
                                            <ul className={classes.ul} id={`section-${coll.name}`}>

                                                <ListItem key={`item-${coll.name}-`}>
                                                    <ListItemText primary={`${coll.name} (${coll.value})`} id={`section-${coll.name}`}/>
                                                    <ListItemSecondaryAction id={`section-${coll.name}`}>

                                                        <IconButton aria-label="delete"
                                                                    id={index}
                                                                    key={coll}
                                                                    onClick={this.deleteCollegeHandler}

                                                        >
                                                            <DeleteOutlineIcon
                                                                id={index}
                                                                key={coll}
                                                                onClick={this.deleteCollegeHandler}/>
                                                        </IconButton>

                                                    </ListItemSecondaryAction>
                                                </ListItem>

                                            </ul>
                                        </li>
                                    ))}
                                </List>
                                <Button variant="contained" className={classes.bottom} color="secondary"
                                        onClick={() => {
                                            this.setState({
                                                collegeModal: true
                                            })
                                        }}>
                                    افزودن
                                </Button>
                            </Grid>

                            <Grid item xs={12} sm={6}>
                                <List className={classes.lRoot} subheader={<li/>}>
                                    <ListSubheader>{`ترم ها`}</ListSubheader>
                                    {this.props.parentProps.semester.map((coll) => (
                                        <li key={`section-${coll}`} className={classes.listSection}>
                                            <ul className={classes.ul}>

                                                <ListItem key={`item-${coll}-`}>
                                                    <ListItemText primary={`${coll}`}/>
                                                    <ListItemSecondaryAction>

                                                        <IconButton aria-label="delete"
                                                                    id={coll}
                                                                    onClick={this.deleteSemesterHandler}

                                                        >
                                                            <DeleteOutlineIcon
                                                                id={coll}
                                                                onClick={this.deleteSemesterHandler}/>
                                                        </IconButton>

                                                    </ListItemSecondaryAction>
                                                </ListItem>

                                            </ul>
                                        </li>
                                    ))}
                                </List>
                                <Button variant="contained" onClick={() => {
                                    this.setState({
                                        semesterModal: true
                                    })
                                }} className={classes.bottom} color="secondary">
                                    افزودن
                                </Button>
                            </Grid>

                        </Grid>
                        <Button onClick={this.commitDynamicData} type="submit" variant="contained"
                                className={classes.bottom}
                                color="primary">
                            ثبت
                        </Button>
                    </Paper>

                </Grid>

                <div>
                    <Modal
                        aria-labelledby="transition-modal-title"
                        aria-describedby="transition-modal-description"
                        className={classes.modal}
                        open={this.state.semesterModal}
                        onClose={() => {
                            this.setState({
                                semesterModal: false
                            })
                        }}
                        closeAfterTransition
                        BackdropComponent={Backdrop}
                        BackdropProps={{
                            timeout: 500,
                        }}
                    >
                        <Fade in={this.state.semesterModal}>
                            <div className={classes.paper2}>

                                <Grid container justify="center" className={classes.vertical} spacing={3}>
                                    <Grid item xs={3}>
                                        <TextField
                                            value={this.state.semesterValue1}
                                            onChange={(event) => {
                                                this.setState({
                                                    semesterValue1: event.target.value
                                                })
                                            }}
                                            inputProps={{min: 0, style: {textAlign: 'center'}}}
                                            id="standard-basic"/>
                                    </Grid>
                                    -
                                    <Grid item xs={3}>
                                        <TextField
                                            value={this.state.semesterValue2}
                                            onChange={(event) => {
                                                this.setState({
                                                    semesterValue2: event.target.value
                                                })
                                            }}
                                            inputProps={{min: 0, style: {textAlign: 'center'}}}
                                            id="standard-basic"/>
                                    </Grid>
                                    -
                                    <Grid item xs={3}>
                                        <TextField
                                            value={this.state.semesterValue3}
                                            onChange={(event) => {
                                                this.setState({
                                                    semesterValue3: event.target.value
                                                })
                                            }}
                                            inputProps={{min: 0, style: {textAlign: 'center'}}}
                                            id="standard-basic"/>
                                    </Grid>
                                </Grid>

                                <Grid xs={12} item className={classes.topMargin}>
                                    <Button type="submit" variant="contained" classes={classes.topMargin}
                                            color="primary" onClick={() => {
                                        if (this.state.semesterValue1 !== undefined &&
                                            this.state.semesterValue2 !== undefined &&
                                            this.state.semesterValue3 !== undefined) {
                                            let value = this.state.semesterValue1 + "-" +
                                                this.state.semesterValue2 + "-" + this.state.semesterValue3;
                                            let pState = [...this.props.parentProps.semester];
                                            pState.push(value);
                                            this.props.parentProps.setSemester(pState);
                                            this.setState({
                                                semesterModal: false,
                                                semesterValue1: undefined,
                                                semesterValue2: undefined,
                                                semesterValue3: undefined
                                            })
                                        }
                                    }}>
                                        افزودن
                                    </Button>
                                </Grid>
                            </div>
                        </Fade>
                    </Modal>
                </div>


                <div>
                    <Modal
                        aria-labelledby="transition-modal-title"
                        aria-describedby="transition-modal-description"
                        className={classes.modal}
                        open={this.state.collegeModal}
                        onClose={() => {
                            this.setState({
                                collegeModal: false
                            })
                        }}
                        closeAfterTransition
                        BackdropComponent={Backdrop}
                        BackdropProps={{
                            timeout: 500,
                        }}
                    >
                        <Fade in={this.state.collegeModal}>
                            <div className={classes.paper2}>

                                <Grid container justify="center" className={classes.vertical} spacing={3}>
                                    <Grid item xs={6}>
                                        <TextField
                                            style={{width: "90%"}}
                                            value={this.state.collegeValue1}
                                            onChange={(event) => {
                                                this.setState({
                                                    collegeValue1: event.target.value
                                                })
                                            }}
                                            inputProps={{min: 0, style: {textAlign: 'center'}}}
                                            id="standard-basic"/>
                                    </Grid>

                                </Grid>

                                <Grid xs={12} item className={classes.topMargin}>
                                    <Button type="submit" variant="contained" classes={classes.topMargin}
                                            color="primary" onClick={() => {
                                        if (this.state.collegeValue1 !== undefined) {
                                            let guess = this.guessCollegeDigestName();
                                            console.log(guess)
                                            let value = {
                                                name: this.state.collegeValue1,
                                                value: guess
                                            }
                                            let pState = [...this.props.parentProps.college];
                                            pState.push(value);
                                            this.props.parentProps.setCollege(pState);
                                            this.setState({
                                                collegeModal: false,
                                                collegeValue1: undefined
                                            })
                                        }
                                    }}>
                                        افزودن
                                    </Button>
                                </Grid>
                            </div>
                        </Fade>
                    </Modal>
                </div>

            </div>
        )
    }
}

export default withStyles(styles, {withTheme: true})(ValueManager);
