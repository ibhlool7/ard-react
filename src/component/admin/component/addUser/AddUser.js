import React, {Component} from "react";
import {FormUser, UserTable} from "../../../../constants/constant";
import User from "../../../user/User";
import "./AddUser.css";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from '@material-ui/core/Typography';
import InlineAlert from "../../../alerts/InlineAlert";
import {
    cook,
    header,
    api,
    errors,
    cooking,
    USER,
    ADMIN,
    FieldsNames
} from "../../../../constants/constant.js";
import Axios from "axios";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import Alert from "@material-ui/lab/Alert";
import {withStyles} from "@material-ui/core/styles";
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import * as constant from "../../../../constants/constant";
import FormControl from "@material-ui/core/FormControl";
import Select from '@material-ui/core/Select';
import InputLabel from "@material-ui/core/InputLabel";
import * as axios from "axios";


const styles = (theme) => ({
    form: {
        width: "100%",
        justifyContent: "center",
        textAlign: "center",
        margin: "15px"
    },
    fields: {
        width: "70%",
        direction: "rtl",
        margin: "10px"
    },
    paper: {
        width: "50%"
    },
    root: {
        margin: "auto"
    },
    bottom: {
        width: "1۵%",
        direction: "rtl",
        margin: "10px"
    },
    selectBox: {
        margin: "10px"
    }
});


class AddUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comp: UserTable,
            open: false,
            field: FieldsNames[0]
        };
    }

    sendExcel = (event) => {
        event.preventDefault()
        let formData = new FormData();
        const file = event.target.files[0];
        formData.append("user.csv", file);
        axios.put(`${api}api/v1/upload/user.csv`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            this.setState({
                success: true,
                error: null,
                open: true
            });
        }).catch(err => {
            console.log(err)
        })
    }
    onSubmitHandler = event => {
        event.preventDefault();
        const national_code = event.target.elements.national_code.value;
        const first_name = event.target.elements.first_name.value;
        const last_name = event.target.elements.last_name.value;
        const shaba = event.target.elements.shaba.value;
        const group = event.target.elements.group.value;
        if (
            national_code === "" ||
            first_name === "" ||
            (last_name === "") | (shaba === "") | (group === "")
        ) {
            this.setState({
                error: "لطغا همه فیلد ها را تکمیل نمایید"
            });
            return;
        }
        const data = {
            national_code: national_code,
            first_name: first_name,
            last_name: last_name,
            shaba: shaba,
            group: group
        };
        Axios.post(
            `${api}api/v1/user/push/`,
            data,
            header(this.props.parentProps.auth)
        )
            .then(res => {
                if (res.status === 201) {
                    this.setState({
                        success: true,
                        error: null,
                        open: true
                    });
                }
                this.refs.form.reset();
            })
            .catch(err => {
                if (err.response.status === 409) {
                    this.setState({
                        error: "کاربر با این شماره ملی قبلا ثبت گردیده است"
                    });
                } else {
                    this.setState({
                        error: err
                    });
                }
            });
    };

    render() {
        const {classes} = this.props;

        return (
            <React.Fragment>
                <React.Fragment>
                    <Grid container spacing={1} className={classes.root}>
                        <Paper className={classes.paper}>
                            <form onSubmit={this.onSubmitHandler} className={classes.form} noValidate autoComplete="off"
                                  ref="form">
                                <Grid item xs={12} sm={12}>
                                    <Typography variant={"h6"}>ایجاد کاربر جدید</Typography>
                                </Grid>
                                <Grid item xs={12} sm={12}>
                                    <TextField required={true} id="national_code" className={classes.fields}
                                               label="کد ملی"/>
                                </Grid>

                                <Grid item xs={12} sm={12}>
                                    <TextField required={true} id="first_name" className={classes.fields} label="نام"/>

                                </Grid>
                                <Grid item xs={12} sm={12}>
                                    <TextField required={true} id="last_name" className={classes.fields}
                                               label="نام خانوادگی"/>
                                </Grid>
                                <Grid item xs={12} sm={12}>
                                    <TextField required={true} id="shaba" className={classes.fields} label="شبا"/>

                                </Grid>
                                <Grid item xs={12} sm={12} className={classes.selectBox}>
                                    <InputLabel htmlFor="age-native-simple">گروه</InputLabel>
                                    <Select
                                        native
                                        id={"group"}
                                        value={this.state.field}
                                        onChange={(e) => {
                                            this.setState({
                                                field: e.target.value
                                            })
                                        }}>
                                        {this.props.parentProps.college.map((item) =>
                                            (<option value={item.value}>{item.name}</option>)
                                        )}

                                    </Select>
                                </Grid>
                                <Button type="submit" variant="contained" className={classes.bottom} color="primary">
                                    ثبت
                                </Button>

                                <Grid item xs={12} sm={12}>

                                    <input
                                        onChange={this.sendExcel}
                                        className={classes.input}
                                        style={{display: 'none'}}
                                        id="raised-button-file"
                                        multiple
                                        type="file"
                                        accept=".csv"
                                    />
                                    <label htmlFor="raised-button-file">
                                        <Button variant="contained" component="span" className={classes.bottom}
                                                color="secondary">
                                            آپلود اکسل
                                        </Button>
                                    </label>
                                </Grid>


                            </form>
                        </Paper>
                    </Grid>
                </React.Fragment>
                {(
                    <Snackbar
                        onClose={() => {
                            this.setState({
                                open: false
                            });
                        }}
                        open={this.state.open}
                        autoHideDuration={5000}
                    >
                        <Alert severity="success">کاربر با موفقیت ثبت گردید</Alert>
                    </Snackbar>
                )}
                {this.state.error && <InlineAlert error={this.state.error}/>}
                {this.state.success && (
                    <Snackbar
                        onClose={() => {
                            this.setState({
                                open: false
                            });
                        }}
                        open={this.state.open}
                        autoHideDuration={5000}
                    >
                        <Alert severity="success">کاربر با موفقیت ثبت گردید</Alert>
                    </Snackbar>
                )}
            </React.Fragment>
        )
            ;
    }
}

export default withStyles(styles, {withTheme: true})(AddUser);
