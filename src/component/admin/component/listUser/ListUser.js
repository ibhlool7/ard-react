import React, {Component} from "react";
import "./ListUser.css";
import InlineAlert from "../../../alerts/InlineAlert";
import Loading from "../loading/Loading";
import Axios from "axios";
import InvoiceList from "./invoiceList/InvoiceList";
import {
    cook,
    header,
    api,
    errors,
    cooking,
    USER,
    ADMIN,
    FieldsNames
} from "../../../../constants/constant";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {withStyles} from "@material-ui/core/styles";
import LinearProgress from '@material-ui/core/LinearProgress';
import Fab from "@material-ui/core/Fab";
import * as axios from "axios";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";

const cycle = [
    0, //Loading
    1, //usersList
    2 //userInvoice
];


const styles = (theme) => ({
    root: {
        height: '100vh'
    },
    image: {
        // backgroundImage: 'url(https://source.unsplash.com/random)',
        backgroundImage: `url(/banner.png)`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundColor: theme.palette.primary.dark
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(16),
        backgroundColor: theme.palette.secondary.dark,
    },
    form: {
        width: '99%', // Fix IE 11 issue.
        padding: theme.spacing(10),
        margin: '10px',
        alignItems: "center",
        alignContent: "center",
        textAlign: "center",
        justifyContent: "center",
        direction: 'rtl'
    },
    cell: {
        textAlign: "center"
    }, buttomCell: {
        textAlign: "center",
        direction: "ltr"
    },
    submit: {
        margin: theme.spacing(5, 0, 5),
    },
    searchPart: {
        paddingRight: "50px",
        paddingLeft: "50px",
        paddingTop: "10px",
        paddingBottom: "10px",
        width: "100%",
        display: "block"
    }
});

class ListUser extends Component {
    constructor(props) {
        super(props);
        this.state = {error: null, step: 0, open: false, search : ""};
    }

    userSelection = index => {
        this.setState({pos: index, step: 2}); // this implementing is not appropriate for searching
    };

    deleteHandler = code => {
        Axios.delete(
            `${api}api/v1/user/push/${code}`,
            {},
            header(this.props.parentProps.auth)
        )
            .then(res => {
                if (res.status === 200) {
                    this.setState(currentState => ({
                        open: true,
                        data: currentState.data.filter(item => {
                            return item.national_code !== code;
                        })
                    }));
                }
            })
            .catch(e => {
                this.setState({error: e});
            });
    };
    excelHandler = (event) => {
        event.preventDefault()
        let formData = new FormData();
        const file = event.target.files[0];
        formData.append("invoice.csv", file);
        axios.put(`${api}api/v1/upload/invoice.csv`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            this.setState({
                success: true,
                error: null,
                open: true
            });
        }).catch(err => {
            console.log(err)
        })
    }

    componentDidMount() {
        Axios.get(`${api}api/v1/user/users/`)
            .then(res => {
                this.setState({
                    step: 1,
                    data: res.data
                });
            })
            .catch(e => {
                try {
                    this.setState({
                        error: e.response.data
                    });
                } catch (error) {
                    this.setState({
                        error: error
                    });
                }
            });
    }

    search = (value)=>{
        this.setState({
            search: value
        })
        let list = document.getElementsByTagName("tr")
        for (let i = 0; i<list.length; i++){

            console.log(list[i].id)
            if(list[i].id === "header"){
                continue
            }

            if (!list[i].textContent.includes(value)){
                list[i].style.display = 'none';
            }else{
                list[i].style.display = 'table-row';
            }
        }
    }
    render() {
        const {classes} = this.props;

        let toRender = null;

        switch (this.state.step) {
            case 0: {
                toRender = <LinearProgress/>;
                break;
            }
            case 1: {
                toRender = (
                    <div className="containertab shadow">
                        <Grid container xl={12} md={12}>
                            <Grid item xl={12} md={12}>
                                <div className={classes.searchPart}>
                                    <TextField style={{width: "100%"}}
                                               id="search"
                                               label="جستجو"
                                               type="text"
                                               InputLabelProps={{
                                                   shrink: true,
                                               }}
                                               value={this.state.search}
                                               onChange={(target)=>{
                                                   this.search(target.target.value)
                                               }
                                               }
                                    />
                                </div>
                            </Grid>


                            <Grid item xl={12} md={12}>
                                <TableContainer>
                                    <Table className={classes.form}>
                                        <TableHead>
                                            <TableRow className={"col-table"} id={"header"}>
                                                <TableCell className={classes.cell}>شماره</TableCell>
                                                <TableCell className={classes.cell}>کد ملی</TableCell>
                                                <TableCell className={classes.cell}>نام</TableCell>
                                                <TableCell className={classes.cell}>نام خانوادگی</TableCell>
                                                <TableCell className={classes.cell}>شماره شبا</TableCell>
                                                <TableCell className={classes.cell}>گروه</TableCell>
                                                <TableCell className={classes.cell}>امکانات</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody className={"col-table"}>
                                            {console.log(typeof this.state.data)}
                                            {console.log(this.state.data)}
                                            {this.state.data.map((item, index) => {
                                                return (
                                                    <TableRow hover key={index}  className={"col-table"}>
                                                        <TableCell className={classes.cell}>
                                                            {index + 1}
                                                        </TableCell>
                                                        <TableCell className={classes.cell}>
                                                            {item.national_code}
                                                        </TableCell>
                                                        <TableCell className={classes.cell}>
                                                            {item.first_name}
                                                        </TableCell>
                                                        <TableCell className={classes.cell}>
                                                            {item.last_name}

                                                        </TableCell>
                                                        <TableCell className={classes.cell}>
                                                            {item.shaba}

                                                        </TableCell>
                                                        <TableCell className={classes.cell}>
                                                            {
                                                                this.props.parentProps.college.map((index) => {
                                                                    if (index.value === item.group) {
                                                                        console.log(item.group)
                                                                        console.log(index.value)
                                                                        return index.name
                                                                    }
                                                                })}

                                                        </TableCell>
                                                        <TableCell className={classes.buttomCell}>

                                                            <ButtonGroup
                                                                color="primary"
                                                                aria-label="outlined primary button group"
                                                            >
                                                                <Button
                                                                    onClick={() => {
                                                                        this.userSelection(index);
                                                                    }}
                                                                >
                                                                    صورت حساب
                                                                </Button>
                                                                <Button
                                                                    onClick={() => {
                                                                        this.deleteHandler(item.national_code);
                                                                    }}
                                                                >
                                                                    حذف
                                                                </Button>
                                                            </ButtonGroup>
                                                        </TableCell>


                                                    </TableRow>
                                                )
                                            })}
                                        </TableBody>
                                    </Table>

                                </TableContainer>
                            </Grid>
                        </Grid>


                        {this.state.open && (
                            <Snackbar
                                onClose={() => {
                                    this.setState({
                                        open: false
                                    });
                                }}
                                open={this.state.open}
                                autoHideDuration={5000}
                            >
                                <Alert severity="success">با موفقیت ذخیره شد</Alert>
                            </Snackbar>
                        )}
                        <input
                            onChange={this.excelHandler}
                            className={classes.input}
                            style={{display: 'none'}}
                            id="raised-button-file"
                            multiple
                            type="file"
                            accept=".csv"
                        />

                        <button
                            className="add">
                            <label htmlFor="raised-button-file">
                                <Fab color="primary" variant="extended"
                                     component="span"
                                     className={("hook_bottom_left")} aria-label="edit">
                                    آپلود اکسل
                                </Fab>
                            </label>
                        </button>


                    </div>
                );
                break;
            }
            case 2: {
                toRender = (
                    <InvoiceList {...this.props} data={this.state.data[this.state.pos]}/>
                );
                break;
            }
            default:
                toRender = <Loading/>;
        }

        return (<div>{toRender}</div>);
    }
}

export default withStyles(styles, {withTheme: true})(ListUser);
