import React, {Component} from "react";
import Loading from "../../loading/Loading";
import NumberFormat from 'react-number-format';
import {
    cook,
    header,
    api,
    errors,
    cooking,
    USER,
    getPureValueFromFormatNumberComponent,
    ADMIN,
    HALF_YEARS, Terms
} from "../../../../../constants/constant";
import Axios from "axios";
import AddInvoice from "./add/AddInvoice";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {withStyles} from "@material-ui/core/styles";
import LinearProgress from '@material-ui/core/LinearProgress';
import Fab from '@material-ui/core/Fab';
import {StepIcon} from "@material-ui/core";
import Add from "@material-ui/core/Icon"
import '../ListUser.css'
import Select from "@material-ui/core/Select";
import * as constant from "../../../../../constants/constant";
import FormControl from "@material-ui/core/FormControl";


const cycle = [
    0, //Loading
    1, //usersList
    2 //userInvoice
];

const styles = (theme) => ({
    root: {
        height: '100vh'
    },
    image: {
        // backgroundImage: 'url(https://source.unsplash.com/random)',
        backgroundImage: `url(/banner.png)`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundColor: theme.palette.primary.dark
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(16),
        backgroundColor: theme.palette.secondary.dark,
    },
    form: {
        width: '99%', // Fix IE 11 issue.
        padding: theme.spacing(10),
        margin: '10px',
        alignItems: "center",
        alignContent: "center",
        textAlign: "center",
        justifyContent: "center",
        direction: 'rtl'
    },
    cell: {
        textAlign: "center"
    },
    submit: {
        margin: theme.spacing(5, 0, 5),
    },
    formGroupDesign: {
        direction: "rtl"
    },
    fab: {
        position: 'absolute',
        left: 0
    },
    headerModal: {
        textAlign: "center"
    },
    modalItem: {
        justifyContent: "center",
        textAlign: "center"
    },
    selectBox: {
        height: theme.spacing(12),
        width: theme.spacing(70),
        justifyContent:"center",
        textAlign:"center"
    }
});

class InvoiceList extends Component {
    constructor(props) {
        super(props);

        this.state = {step: 0, open: false, field: HALF_YEARS[0]};
    }


    onSubmitHandler = event => {
        event.preventDefault();
        const base_salary = getPureValueFromFormatNumberComponent(event.target.elements.base_salary.value);
        const special_awesome = getPureValueFromFormatNumberComponent(event.target.elements.special_awesome.value);
        const judgement_time = getPureValueFromFormatNumberComponent(event.target.elements.judgement_time.value);
        const observe_time = getPureValueFromFormatNumberComponent(event.target.elements.observe_time.value);
        const term = event.target.elements.term.value;
        const teach_time = getPureValueFromFormatNumberComponent(event.target.elements.teach_time.value);
        const user_id = getPureValueFromFormatNumberComponent(this.props.data.national_code);
        Axios.post(
            `${api}api/v1/user/invoice/`,
            {
                base_salary: base_salary,
                special_awesome: special_awesome,
                judgement_time: judgement_time,
                observe_time: observe_time,
                term: term,
                user_id: user_id,
                teach_time: teach_time
            },
            header(this.props.parentProps.auth)
        )
            .then(res => {
                this.setState(currentState => ({
                    show: false,
                    open: true,
                    message: "صورت حساب با موفقیت ثبت گردید",
                    data: [...currentState.data, res.data]
                }))

            })
            .catch(er => {
                if (er.response) {
                    this.setState({error: "مشکلی رخ داده است"});
                }
            });
    };

    deleteHandler = id => {
        Axios.delete(
            `${api}api/v1/report/specific_user_invoices/${id}/`,
            {},
            header(this.props.parentProps.auth)
        )
            .then(res => {

                this.setState(currentState => ({
                    open: true,
                    data: currentState.data.filter(item => {
                        return item.pk !== id;
                    }),
                    message: "صورت حساب با موفقیت حذف گردید"
                }));
            })
            .catch(er => {
                this.setState({
                    error: er
                });
            });
    };

    addHandler = () => {
        this.setState({
            // step: 3
            show: true
        });
    };
    setStep = step => {
        this.setState({step: step});
    };

    getData = () => {
        Axios.get(
            `${api}api/v1/report/specific_user_invoices/?national_code=${this.props.data.national_code}`,
            {},
            header(this.props.parentProps.auth)
        )
            .then(res => {
                this.setState({
                    step: 1,
                    data: res.data === "" ? [] : res.data
                });
            })
            .catch(er => {
            });
    };

    componentDidMount() {
        this.getData();
    }

    render() {
        const {classes} = this.props;

        let toRender = <LinearProgress/>;
        switch (this.state.step) {
            case 0: {
                toRender = <LinearProgress/>;
                break;
            }
            case 1: {
                toRender = (
                    <div className="containertab shadow">
                        <TableContainer>
                            <Table className={classes.form}>
                                <TableHead>
                                    <TableRow className={"col-table"}>
                                        <TableCell className={classes.cell}>شناسه</TableCell>
                                        <TableCell className={classes.cell}>ساعات تدریس</TableCell>
                                        <TableCell className={classes.cell}>ساعات داوری</TableCell>
                                        <TableCell className={classes.cell}>ساعات نظارت</TableCell>
                                        <TableCell className={classes.cell}>ارزش ساعت</TableCell>
                                        <TableCell className={classes.cell}>جمع ساعات</TableCell>
                                        <TableCell className={classes.cell}>جمع هزینه</TableCell>
                                        <TableCell className={classes.cell}>مالیات</TableCell>
                                        <TableCell className={classes.cell}>حقوق خالص</TableCell>
                                        <TableCell className={classes.cell}>پایه حقوق</TableCell>
                                        <TableCell className={classes.cell}>ویژه</TableCell>
                                        <TableCell className={classes.cell}>امکانات</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody className={"col-table"}>
                                    {this.state.data.map((item, index) => {
                                        return (
                                            <TableRow hover key={index} className={"col-table"}>
                                                <TableCell className={classes.cell} name="pk">
                                                    {item.pk}
                                                </TableCell>
                                                <TableCell className={classes.cell}>
                                                    {item.tech_time}
                                                </TableCell>
                                                <TableCell className={classes.cell}>
                                                    {item.judge_time}
                                                </TableCell>
                                                <TableCell className={classes.cell}>
                                                    {item.observing_time}

                                                </TableCell>
                                                <TableCell className={classes.cell}>
                                                    {item.cost_of_time}

                                                </TableCell>
                                                <TableCell className={classes.cell}>
                                                    {item.sum_of_time}

                                                </TableCell>
                                                <TableCell className={classes.cell}>
                                                    {item.gather_cost}

                                                </TableCell>
                                                <TableCell className={classes.cell}>
                                                    {item.pure_salary}

                                                </TableCell>
                                                <TableCell className={classes.cell}>
                                                    {item.base_salary}

                                                </TableCell>
                                                <TableCell className={classes.cell}>
                                                    {item.special}

                                                </TableCell>
                                                <TableCell className={classes.cell}>
                                                    {item.sum_of_time}

                                                </TableCell>
                                                <ButtonGroup
                                                    color="primary"
                                                    aria-label="outlined primary button group"
                                                >
                                                    <Button
                                                        onClick={() => {
                                                            this.deleteHandler(item.pk);
                                                        }}
                                                    >
                                                        حذف
                                                    </Button>
                                                </ButtonGroup>

                                            </TableRow>
                                        )
                                    })}
                                </TableBody>
                            </Table>

                        </TableContainer>

                        <Snackbar
                            onClose={() => {

                                this.setState({
                                    open: false
                                });
                            }}
                            open={this.state.open}
                            autoHideDuration={5000}
                        >
                            <Alert severity="success">{this.state.message}</Alert>
                        </Snackbar>

                        <Dialog

                            open={this.state.show}
                            onClose={() => {
                                this.setState({
                                    show: false
                                });
                            }}
                            aria-labelledby="form-dialog-title"
                        >
                            <DialogTitle id="form-dialog-title" className={classes.headerModal}>صورت حساب
                                جدید</DialogTitle>
                            <DialogContent>
                                <form onSubmit={this.onSubmitHandler}>
                                    <div className={"row row-space"}>
                                        <div className={("col-5")}>
                                            <div className={classes.modalItem}>
                                                <div className="input-group alignment-child">
                                                    <label className="label">حقوق پایه</label>
                                                    <NumberFormat
                                                        className="input--style-4"
                                                        name="base_salary"
                                                        required="true"
                                                        thousandSeparator={true}
                                                        prefix={' ریال'}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-5">
                                            <div className="input-group alignment-child">
                                                <label className="label">ویژه</label>
                                                <NumberFormat name="special_awesome"
                                                              className="input--style-4"
                                                              required="true"
                                                              thousandSeparator={true}
                                                              prefix={' ریال'}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row row-space">
                                        <div className="col-5">
                                            <div className="input-group alignment-child">
                                                <label className="label">ساعات قظاوت</label>
                                                <NumberFormat
                                                    className="input--style-4"
                                                    name="judgement_time"
                                                    required="true"
                                                    thousandSeparator={true}
                                                    prefix={' ساعت '}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-5">
                                            <div className="input-group alignment-child">
                                                <label className="label">ساعات نظارت</label>
                                                <NumberFormat
                                                    className="input--style-4"
                                                    name="observe_time"
                                                    required="true"
                                                    thousandSeparator={true}
                                                    prefix={' ساعت'}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row row-space">
                                        <div className="col-5">
                                            <div className="input-group alignment-child">
                                                <label className="label">ترم</label>
                                                <Select
                                                    native
                                                    className={{"input--style-4": true, [classes.selectBox]:true}}
                                                    value={this.state.field}
                                                    name={"term"}
                                                    onChange={(e) => {
                                                        this.setState({
                                                            field: e.target.value
                                                        })
                                                    }}>
                                                    {this.props.parentProps.semester.map((item) =>
                                                        (<option value={item}>{item}</option>)
                                                    )}

                                                </Select>
                                            </div>
                                        </div>
                                        <div className="col-5">
                                            <div className="input-group alignment-child">
                                                <label className="label">ساعات تدریس</label>
                                                <NumberFormat
                                                    className="input--style-4"
                                                    required="true"
                                                    name="teach_time"
                                                    thousandSeparator={true}
                                                    prefix={' ساعت'}
                                                />
                                            </div>
                                        </div>
                                    </div>

                                    <div className={classes.modalItem}>
                                        <Button variant="contained" color="primary" type="submit">
                                            ثبت
                                        </Button>
                                    </div>
                                </form>
                            </DialogContent>
                        </Dialog>

                        <button
                            className="add">
                            <Fab color="primary" variant="extended" onClick={this.addHandler}
                                 className={("hook_bottom_left")} aria-label="edit">
                                صورت حساب جدید
                            </Fab>
                        </button>
                    </div>
                );
                break;
            }
            case 3: {
                toRender = <AddInvoice {...this.props} setStep={this.setStep}/>;
                break;
            }

            default:
                toRender = <Loading/>;
        }
        return <div>{toRender}</div>;
    }
}

export default withStyles(styles, {withTheme: true})(InvoiceList);
