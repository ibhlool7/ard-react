import React, { Component } from "react";
import "../../../addUser/AddUser.css";
import Axios from "axios";
import {
  cook,
  header,
  api,
  errors,
  cooking,
  USER,
  ADMIN
} from "../../../../../../constants/constant";
import InlineAlert from "../../../../../alerts/InlineAlert";
import NumberFormat from 'react-number-format';


class AddInvoice extends Component {
  constructor(props) {
    super(props);
    this.state = { error: null };

  }
  onSubmitHandler = event => {
    event.preventDefault();
    const base_salary = event.target.elements.base_salary.value;
    const special_awesome = event.target.elements.special_awesome.isNumericString;
    const judgement_time = event.target.elements.judgement_time.value;
    const observe_time = event.target.elements.observe_time.value;
    const term = event.target.elements.term.value;
    const teach_time = event.target.elements.teach_time.value;
    const user_id = this.props.data.national_code;
    Axios.post(
      `${api}api/v1/user/invoice/`,
      {
        base_salary: base_salary,
        special_awesome: special_awesome,
        judgement_time: judgement_time,
        observe_time: observe_time,
        term: term,
        user_id: user_id,
        teach_time: teach_time
      },
      header(this.props.parentProps.auth)
    )
      .then(res => {
        this.props.setStep(1);
      })
      .catch(er => {
        if (er.response) {
          this.setState({ error: er.response.data });
        }
      });
  };
  render() {
    return (
      <div className="page-wrapper p-t-130 p-b-100 font-poppins shadow">
        {this.state.error && <InlineAlert error={this.state.error} />}
        <div className="wrapper wrapper--w680">
          <div className="card card-4">
            <div className="card-body">
              <h2 className="title">Registration Form</h2>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AddInvoice;
