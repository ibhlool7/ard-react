import React, { Component } from "react";
import "./Loding.scss";

class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div
        className="loader__wrap"
        role="alertdialog"
        aria-busy="true"
        aria-live="polite"
        aria-label="Loading…"
      >
        <div className="loader" aria-hidden="true">
          <div className="loader__sq" />
          <div className="loader__sq" />
        </div>
      </div>
    );
  }
}

export default Loading;
