import React, {Component} from "react";
import "./login.css";
import InlineAlert from "../alerts/InlineAlert";
import {
    cook,
    header,
    api,
    errors,
    cooking,
    USER,
    ADMIN
} from "../../constants/constant";
import Axios from "axios";
import {Link} from "react-router-dom";
import Cookies from "universal-cookie";
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Avatar from '@material-ui/core/Avatar';
import {withStyles} from '@material-ui/core/styles';
import {Link as Li} from '@material-ui/core/'
import {styles} from '../signin/SignIn'


function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: ""
        };
    }

    submitHandler = event => {
        event.preventDefault();
        const email = event.target.elements.email.value;
        const code = event.target.elements.code.value;
        const password = event.target.elements.password.value;
        if (password === "" || code === "" || email === "") {
            this.setState({
                error: "please fill the requires"
            });
            return;
        }
        const data = {
            email: email,
            password: password,
            national_code: code
        };
        console.log(data)
        let done = false;
        Axios.post(`${api}api/v1/user/sign-up/`, data, header(null))
            .then(res => {
                console.log(res)
                this.setCookie(res.data["auth"], role);
                this.props.parentProps.setSign(role, res.data["auth"]);
                Axios.post(`${api}api/token/`, data, header(null)).then(res => {
                    console.log(data)
                    this.props.parentProps.setAuth(
                        this.props.parentProps.role,
                        res.data["auth"],
                        res.data['name'],
                        res.data['national_code'],
                        res.data['last_name'],
                        res.data['id'],
                    );
                })

                let role = USER;
                if (res.data["staff"]) {
                    role = ADMIN;
                }
                this.setCookie(res.data["auth"], role);
                this.props.parentProps.setAuth(role, res.data["auth"]);
                done = true;

            })
            .catch(err => {
              //  console.log(err.response.data)
                done = false;
                this.setState({
                    error: err.response.data.detail
                });
            }).finally(() => {
            if (done) {
                this.props.params.history.push("/");
            }
        })
    };
    setFinisher = () => {
        this.props.params.history.push("/");
    }
    setCookie = (auth, role) => {
        const cookies = new Cookies();
        cookies.set(cook, cooking(auth, role, {path: "/"}));
    };

    render() {
        const {classes} = this.props;
        if (this.props.role) {
            return <div></div>
        }
        return (
            <div id="parent">

                <Grid container component="main" className={classes.root}>
                    <CssBaseline/>
                    <Grid item xs={false} sm={4} md={8} className={classes.image}/>
                    <Grid item xs={12} sm={8} md={4} component={Paper} elevation={6} square>
                        <div className={classes.paper}>
                            <Avatar className={classes.avatar} src="/arm.jpg">

                            </Avatar>
                            <Typography component="h1" variant="h5">
                                ثبت نام
                            </Typography>
                            <form className={classes.form} onSubmit={this.submitHandler}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="code"
                                    label="کد ملی"
                                    name="code"
                                    autoFocus
                                />
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="email"
                                    label="ایمیل"
                                    type="email"
                                    id="email"
                                    autoComplete="email"
                                />
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password"
                                    label="رمز ورود"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                />

                                <Link to="/sign-in">
                                    <Li className="form-group">قبلا ثبت نام کرده ام</Li>
                                </Link>
                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}
                                >
                                    ثبت نام
                                </Button>

                                <Box mt={5}>
                                    <Copyright/>
                                </Box>
                            </form>
                        </div>
                    </Grid>
                </Grid>
                {this.state.error && <InlineAlert error={this.state.error}/>}
            </div>
        );
    }
}

export default withStyles(styles, {withTheme: true})(Login);
