import "./SignIn.css";
import React, {Component} from "react";
import InlineAlert from "../alerts/InlineAlert";
import {
    cook,
    header,
    api,
    errors,
    cooking,
    USER,
    ADMIN
} from "../../constants/constant";
import Axios from "axios";
import {Link} from "react-router-dom";
import Cookies from "universal-cookie";
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Avatar from '@material-ui/core/Avatar';
import {withStyles} from '@material-ui/core/styles';
import {Link as Li} from '@material-ui/core/'

export const styles = (theme) => ({
    root: {
        height: '100vh'
    },
    image: {
        // backgroundImage: 'url(https://source.unsplash.com/random)',
        backgroundImage: `url(/banner.png)`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundColor: theme.palette.primary.dark
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(16),
        backgroundColor: theme.palette.secondary.dark,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        alignItems: "center"
    },
    submit: {
        margin: theme.spacing(5, 0, 5),
    }
});

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {' © '}
            <a color="inherit" href="https://micrommer.herokuapp.com//">
                Micrommer
            </a>
        </Typography>
    );
}

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: ""
        };
    }

    setFinisher = () => {
        this.props.params.history.push("/");

    }
    submitHandler = event => {
        event.preventDefault();
        const code = event.target.elements.code.value;
        const password = event.target.elements.password.value;
        if (password === "" || code === "") {
            this.setState({
                error: "please fill the requires"
            });
            return;
        }
        const data = {
            password: password,
            nationalCode: code
        };
        let done = false;
        Axios.post(`${api}api/token/`, data, header(null))
            .then(res => {
                this.setState({
                    error: null
                })
                let role = USER;
                if (res.data["staff"]) {
                    role = ADMIN;
                }
                console.log(this.state)
                try {
                    this.props.parentProps.setAuth(role, res.data["auth"],
                        res.data['name'],
                        res.data['national_code'],
                        res.data['last_name'],
                        res.data['id'],
                        res.data["shaba"],
                        this.setFinisher);
                    this.setCookie(res.data["auth"], role, res.data['name'],
                        res.data['national_code'],
                        res.data['last_name'],
                        res.data['id'],
                        res.data["shaba"]);

                } catch (error) {
                    console.log(error);
                }
                done = true;
            })
            .catch(err => {
                done = false;
                console.log(err);
                try {
                    this.setState({
                        error: err.response.data.detail
                    });
                } catch (error) {
                    this.setState({
                        error: errors[0]
                    });
                }
            })
            .finally(() => {
                if (done) {
                    this.props.params.history.push("/");
                }
                // this.props.params.history.push("/");

            });
    };

    setCookie = (auth, role, name, nationalName, lastName, id, shaba) => {
        const cookies = new Cookies();
        cookies.set(cook, cooking(auth, role, name, nationalName, lastName, id, shaba, {path: "/"}));
    };

    render() {
        const {classes} = this.props;
        if (this.props.role) {
            return <div></div>
        }

        return (
            <div id="parent">
                <Grid container component="main" className={classes.root}>
                    <CssBaseline/>
                    <Grid item xs={false} sm={4} md={8} className={classes.image}/>
                    <Grid item xs={12} sm={8} md={4} component={Paper} elevation={6} square>
                        <div className={classes.paper}>
                            <Avatar className={classes.avatar} src="/arm.jpg">

                            </Avatar>
                            <Typography component="h1" variant="h5">
                                ورود به حساب
                            </Typography>
                            <form className={classes.form} onSubmit={this.submitHandler}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="code"
                                    label="کد ملی"
                                    name="code"
                                    autoFocus
                                />
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password"
                                    label="رمز ورود"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                />

                                {/*<Link to="/login">*/}
                                {/*    <Li className="form-group"> ثبت نام</Li>*/}
                                {/*</Link>*/}
                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}
                                >ورود</Button>

                                <Box mt={5}>
                                    <Copyright/>
                                </Box>
                            </form>
                        </div>
                    </Grid>
                </Grid>
                {this.state.error && <InlineAlert error={this.state.error}/>}
            </div>
        );
    }
}

export default withStyles(styles, {withTheme: true})(SignIn);
