import React from "react";

export default function MyComponent() {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" preserveAspectRatio="none"
             viewBox="0 0 1440 270">
            <path fill="#3F51B5" fill-opacity="0.8" d="M0,192L1440,32L1440,320L0,320Z"></path>
        </svg>
    )
}