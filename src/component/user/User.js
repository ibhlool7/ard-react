import React, {useState, useEffect} from "react";
import Cookies from "universal-cookie";
import {
    cook,
    header,
    api,
    errors,
    cooking,
    USER,
    ADMIN,
    userDomainPath,
    Terms, reportDomainPath
} from "../../constants/constant";
import "./User.css";
import Select from '@material-ui/core/Select';
import {Box, colors} from "@material-ui/core"
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import Container from "@material-ui/core/Container"
import TableBody from '@material-ui/core/TableBody';
import Axios from "axios";
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import Black from "@material-ui/core/colors/blue";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import LinearProgress from '@material-ui/core/LinearProgress';
import InlineAlert from "../alerts/InlineAlert";
import {Page, Text, View, Document, StyleSheet} from '@react-pdf/renderer';
import {PDFViewer} from '@react-pdf/renderer';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import * as constant from "../../constants/constant";
import {theme} from "../../constants/Theme";
import Toolbar from "@material-ui/core/Toolbar";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import PaymentIcon from '@material-ui/icons/Payment';
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import SchoolIcon from '@material-ui/icons/School';
import CodeIcon from '@material-ui/icons/Code';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import TelegramIcon from '@material-ui/icons/Telegram';
import AndroidIcon from '@material-ui/icons/Android';
import MapIcon from '@material-ui/icons/Map';
import Redirect from "react-router-dom/es/Redirect";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles({
    table: {
        marginTop: "10px",
        width: '72%',
        margin: "auto",
        minWidth: 650,
        // borderColor: '#000000',
        // borderWidth: '1px',
        // borderStyle: "solid"
    },
    row: {
        borderColor: `#000000`,
        borderWidth: `10`
    },
    tableCell: {
        display: "block"
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        border: '2px solid #000',
        width: '50%',
        height: '50%'
    },
    root: {
        textAlign: "right",
        direction: "rtl",
        flexGrow: 1,
    },
    bottomBox: {
        textAlign: "left",
        marginTop: "25px"
    },
    text: {
        textAlign: "right"
    },
    menuButton: {
        marginRight: theme.spacing(1), cursor: "initial"
    },
    middleTitle: {
        marginLeft: 30,
        marginRight: 20,
        cursor: "pointer",
        fontWeight: "bolder",
        fontSize: 20,
        color: theme.palette.grey.A100
    }, title: {
        marginLeft: 30,
        marginRight: 5,
        cursor: "pointer",
        fontSize: 15,
        fontWeight: "bold",
        color: "#fff"

    }, lastTitle: {
        color: "#fff",
        marginLeft: 30,
        marginRight: 15,
        fontSize: 15,
        cursor: "pointer",
        fontWeight: "bold"
    }, hide: {
        flexGrow: 1,
    }, logOut: {
        fontSize: 15,
        cursor: "pointer",
        fontWeight: "normal",
        color: "black",
        background: "white"
    },
    cursor: {
        cursor: "pointer"
    },
    appBar: {
        // color : "#6B79C6"
    },
    h5l: {
        fontWeight: "normal",
        fontSize: 20
    },
    rootFooter: {
        height: 270,
        width: "100%",
        marginTop: "100px",
        backgroundColor: "#F8F8F8",
        backgroundImage: `url(/Vector.png)`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "100% 100%"
    },
    refList: {
        width: "70%",
        marginTop: 15,
        marginLeft: 40,
        color: theme.palette.primary.dark
    }
});


function User(props) {
    const [halfYear, setHalfYear] = useState(Terms[0])
    const [error, setError] = useState(null)
    const [loading, setLoading] = useState(true)
    const [data, setData] = useState(null)
    const [disable, setDisable] = useState(false)
    const [open, setOpen] = useState(false);
    const [pass, setPass] = useState("");
    const [secPass, setSecPass] = useState("");
    const [erPass, setErrPass] = useState("");
    const [logOut, setLogOut] = useState(false);
    const [alert, setAlert] = useState(false);
    const auth = "auth" in props.parentProps && props.parentProps.auth || (new Cookies()).get(cook);
    if (!auth) {
        setError("مشکلی در ورود شما رخ داده لطفا دوباره وارد شوید")
    }
    const classes = useStyles()

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setPass("");
        setSecPass("");
        setErrPass("");
    };

    function getData() {
        const data = {
            term: halfYear,
            national_code: props.parentProps.nationalName
        }
        Axios.post(`${api + reportDomainPath}salary/`, data, header(auth))
            .then(res => {
                setData(res.data)
                setError(null)
                setLoading(false)
                setAlert(false);
                if (!res.data){
                    setAlert(true);
                }
            })
            .catch(err => {
                setData(null)
                setAlert(true);
            })
    }

    const setNewPassword = () => {
        if (pass !== secPass) {
            setErrPass("کلمه عبور مطابقت ندارد")
            return
        }
        let data = {
            national_code: props.parentProps.nationalName,
            pass: pass
        }

        Axios.post(`${api}api/new-pass/`, data, header(props.parentProps.auth)).then(res => {
            setOpen(false)
        }).catch(er => {
            setError("خطا")
        }).finally(() => {
            setPass("");
            setSecPass("");
            setErrPass("");
            setOpen(false)
        })

    }


    function getInit() {
        Axios.get(`${api}api/init/`)
            .then(res => {
                props.parentProps.dynamicInit(res.data)
            })
    }

    useEffect(() => {
        getInit();
        getData();
        console.log(props.parentProps)
    }, [halfYear]);

    window.onafterprint = () => {
        document.getElementById("footer").hidden = false
        document.getElementById("appBar").hidden = false
        document.getElementById("selectBox").hidden = false
        document.getElementById("printBtn").hidden = false
        // document.getElementById("passBtn").hidden = false

    }
    window.onbeforeprint = () => {
        document.getElementById("footer").hidden = true
        document.getElementById("appBar").hidden = true
        document.getElementById("selectBox").hidden = true
        document.getElementById("printBtn").hidden = true
        // document.getElementById("passBtn").hidden = true
    }

    function parseHalfYear(){
        let array = halfYear.split("-")
        let res;
        switch (array[2]){
            case "1" : {
                res = "نیم سال اول";
                break
            }
            case "2" : {
                res = "نیم سال دوم";
                break;
            }
            case "3":{
                res = "تابستان";
                break
            }
            default : {
                res = ""
            }
        }
        return  res + " " +array[1] + " - " + array[0]
    }

    return (
        <div>
            {/* log out */}
            {logOut && <Redirect to={"/sign-in"}/>}
            <div className={classes.root} id="div1">
                <div id="div">
                    <AppBar position="static" id="appBar">
                        <Toolbar className={classes.appBar} id="toolBar">
                            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                                <PaymentIcon/>
                            </IconButton>
                            <Typography id="samane" variant="h6" className={classes.title}>
                                سامانه فیش حقوقی
                            </Typography>

                            {/*<Button variant={"text"} className={classes.middleTitle}>*/}
                            {/*    فیش*/}
                            {/*</Button>*/}
                            <Button variant="text" className={classes.lastTitle} onClick={() => {
                                setOpen(true);
                            }}>
                                تغییر رمز عبور
                            </Button>
                            <Typography variant="h6" className={classes.hide}>
                            </Typography>

                            <Button id="logOut" color="inherit" variant={"contained"}
                                    className={classes.logOut} onClick={() => {
                                const cookies = new Cookies();
                                cookies.remove(cook);
                                setLogOut(true);
                            }

                            }>خروج</Button>
                        </Toolbar>
                    </AppBar>
                </div>
            </div>

            <div id="printable">
                <Container maxWidth="lg" style={{direction: "rtl", marginTop: 100, textAlign: "right"}}>
                    <Typography variant={"h4"} align={"center"}>دانشگاه اردکان</Typography>
                    <Typography color={theme.palette.grey["900"]} style={{direction: "rtl", marginTop: 10}}
                                variant={"h5"}
                                align={"center"}>فیش حقوقی</Typography>
                    <hr/>

                </Container>
            </div>
            {error && <InlineAlert error={error}/>}
            {loading && <LinearProgress/> ||
            <Container maxWidth="lg" className={classes.root}>

                <TableContainer>

                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell colSpan={5} style={{borderColor: "white", padding: "0", paddingBottom: 10}}>
                                    <Grid container xl={12} md={12} style={{alignItems: "baseline"}}>
                                        <Grid item xl={1} md={1} style={{textAlign: "right"}}>
                                            <FormControl variant="outlined" id="selectBox" style={{direction: "rtl"}}>
                                                <InputLabel id="demo-simple-select-outlined-label"
                                                            style={{direction: "rtl"}}>ترم</InputLabel>
                                                <Select
                                                    labelId="demo-simple-select-outlined-label"
                                                    id="demo-simple-select-outlined"
                                                    value={halfYear}
                                                    onChange={(value) => {
                                                        setHalfYear(value.target.value)
                                                    }}
                                                    label="term"
                                                >
                                                    {props.parentProps.semester && props.parentProps.semester.map((item) => {
                                                        return <MenuItem value={item}>{item}</MenuItem>
                                                    })
                                                    }

                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xl={10} sm={10} md={10}>

                                        </Grid>
                                        <Grid item xl={1} md={1}>
                                            <div className={classes.bottomBox}>
                                                <Button variant={"contained"} color="primary" id="printBtn"
                                                        onClick={() => {
                                                            window.print()
                                                        }}>پرینت</Button>

                                            </div>
                                        </Grid>

                                    </Grid>

                                </TableCell>

                            </TableRow>
                            <TableRow style={{paddingBottom: 50}}>
                                <Typography variant={"body1"} style={{marginTop:20}} align={"right"}>نام و نام خانوادگی
                                    : {props.parentProps.name + " " + props.parentProps.lastName}</Typography>
                                <Typography variant={"body1"} align={"right"}>کد ملی
                                    : {props.parentProps.nationalName} </Typography>
                                <Typography variant={"body1"}
                                            align={"right"}>ترم : {parseHalfYear()}</Typography>
                                <Typography variant={"body1"}
                                            align={"right"} style={{paddingBottom: 10}}>شماره شبا
                                    : {props.parentProps.shaba
                                    && props.parentProps.shaba.includes(".")
                                    && props.parentProps.shaba.split(".")[0]
                                    || props.parentProps.shaba}</Typography>
                            </TableRow>


                            <TableRow>
                                <TableCell className={"col-table"} align="center">جمع ساعات حق التدریس</TableCell>
                                <TableCell className={"col-table"} align="center">نرخ هر ساعت</TableCell>
                                <TableCell className={"col-table"} align="center">ناخالص</TableCell>
                                <TableCell className={"col-table"} align="center">مالیات</TableCell>
                                <TableCell className={"col-table"} align="center">خالص پرداختی</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <TableCell className={"col-table"} align="center">{data && data.sum_of_time}</TableCell>
                                <TableCell className={"col-table"} align="center">{data && data.cost_of_time}</TableCell>
                                <TableCell className={"col-table"} align="center">{data && data.gather_cost}</TableCell>
                                <TableCell className={"col-table"} align="center">{data && data.tax}</TableCell>
                                <TableCell className={"col-table"} align="center">{data && data.pure_salary}</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>
                {alert && <div  style={{ display:"block" ,alignItems:"center", textAlign: "-webkit-center", alignContent:"center"}}>
                    <Alert style={{marginTop: 20, direction:"rtl", width :150}} severity="error">یافت نشد</Alert>
                </div >}

                <Dialog open={open} className={classes.text} onClose={handleClose} style={{direction: "ltr"}}
                        aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">تغییر رمز عبور</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {erPass}
                        </DialogContentText>
                        <TextField
                            className={classes.text}
                            autoFocus
                            margin="dense"
                            id="pass"
                            value={pass}
                            onChange={event =>
                                setPass(event.target.value)
                            }
                            label="رمز عبور جدید"
                            fullWidth
                        />
                        <TextField
                            margin="dense"
                            id="secPass"
                            value={secPass}
                            onChange={(event =>
                                    setSecPass(event.target.value)
                            )}
                            label="تکرار رمز عبور جدید"
                            fullWidth
                        />
                    </DialogContent>
                    <DialogActions style={{direction: "rtl"}}>
                        <Button onClick={handleClose} color="primary">
                            لغو
                        </Button>
                        <Button onClick={setNewPassword} color="primary">
                            تایید
                        </Button>
                    </DialogActions>
                </Dialog>


            </Container>
            }
            {/*  footer  */}

            <footer className={classes.rootFooter} id="footer">
                {/* horizontal */}
                <Grid container xl={12} md={12}>
                    <Grid xl={6} md={6}>

                        {/* Vertical */}

                        <Grid container justify="space-around" direction="column" md={12} xl={12}>
                            <Grid md={6} xl={6}>
                                <List className={classes.refList} component="nav" aria-label="main mailbox folders">
                                    <ListItem button
                                              onClick={(event => {
                                                  event.preventDefault()
                                                  window.open('https://ardakan.ac.ir/', '_self', false)
                                              })}
                                    >
                                        <ListItemIcon>
                                            <SchoolIcon style={{fill: theme.palette.primary.main}}/>
                                        </ListItemIcon>
                                        <ListItemText
                                            primary={<Typography type="body2"
                                                                 style={{color: theme.palette.primary.main}}>دانشگاه
                                                اردکان</Typography>} color={theme.palette.primary.main}
                                            style={{color: "red"}}/>
                                    </ListItem>
                                    <ListItem button
                                              onClick={(event => {
                                                  event.preventDefault()
                                                  window.open('https://micrommer.herokuapp.com/', '_self', false)
                                              })}>
                                        <ListItemIcon>
                                            <CodeIcon style={{fill: theme.palette.primary.main}}/>
                                        </ListItemIcon>
                                        <ListItemText
                                            primary={<Typography type="body2"
                                                                 style={{color: theme.palette.primary.main}}>توسعه
                                                دهنده</Typography>}/>
                                    </ListItem>
                                </List>
                            </Grid>

                            <Grid md={6} xl={6}>
                                {/* Internal Horizontal View */}
                                <Grid container md={12} xl={12} style={{marginLeft: 40}}>

                                    <Grid item md={3} xl={2}>
                                        <LinkedInIcon style={{
                                            fill: theme.palette.grey.A100,
                                            width: 61,
                                            height: 61,
                                            cursor: "pointer"
                                        }}
                                                      onClick={(event => {
                                                          event.preventDefault()
                                                          window.open('https://www.linkedin.com/school/ardakan-university/', '_self', false)
                                                      })}/>
                                    </Grid>

                                    <Grid item md={3} xl={2}>
                                        <AndroidIcon style={{
                                            fill: theme.palette.grey.A100,
                                            width: 45,
                                            height: 61,
                                            cursor: "pointer"

                                        }}
                                                     onClick={(event => {
                                                         event.preventDefault()
                                                         window.open('https://ardakan.ac.ir/page-Show/fa/0/form/pId3277', '_self', false)
                                                     })}/>
                                    </Grid>

                                </Grid>
                            </Grid>

                        </Grid>

                    </Grid>

                    <Grid xl={6} md={6}>

                        <div style={{
                            marginTop: 40,
                            direction: "rtl",
                            textAlign: "right",
                            color: theme.palette.grey.A100,
                            marginRight: 40
                        }}><strong>راه‌های
                            ارتباطی با دانشگاه اردکان</strong>
                            <br/><span>آدرس:</span> اردکان - بلوار
                            آیت‌الله خاتمی(ره) - دانشگاه اردکان <br/>
                            <span>کدپستی:</span> ۹۵۴۹۱-۸۹۵۱۸ <span
                            >فکس:&nbsp;</span>۳۲۲۴۸۳۸۴-۰۳۵ <span
                            >رایانامه:</span> info@ardakan.ac.ir
                        </div>

                    </Grid>
                </Grid>

                <div style={{direction: "rtl", textAlign: "center", color: theme.palette.grey.A100, fontSize: 15}}>
                    <p> © کلیه حقوق مادی و معنوی این پورتال برای دانشگاه اردکان محفوظ می‌باشد.
                    </p>
                </div>

            </footer>

        </div>
    );
}


export default User;
