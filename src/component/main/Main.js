import React, {Component} from "react";
import {Route, Redirect} from "react-router-dom";
import {cook, ADMIN, USER} from "./../../constants/constant";
import Cookies from "universal-cookie";
import Login from "../login/Login";
import Home from "../home/Home";
import SignIn from "../signin/SignIn";
import './Main.css'
import Axios from "axios";
import {api} from "../../constants/constant";


class Main extends Component {
    constructor(props) {
        super(props);
        const cookies = new Cookies();
        let json = cookies.get(cook);
        if (
            json === undefined ||
            json === null ||
            json["auth"] === undefined ||
            json["role"] === undefined ||
            json["auth"] === "" ||
            json["role"] === ""
        ) {
            this.state = {
                auth: null,
                role: null
            };
        } else {
            this.state = {
                auth: json["auth"],
                role: json["role"],
                name: json["name"],
                nationalName: json["nationalName"],
                lastName: json["lastName"],
                id: json["id"],
                shaba: json["shaba"]

            };
        }
    }

    setAuth = (role, auth, name, nationalName, lastName, id, shaba, func) => {
        console.log(shaba)
        this.setState({
            auth: auth,
            role: role,
            name: name,
            nationalName: nationalName,
            lastName: lastName,
            id: id,
            shaba: shaba

        }, () => {
            func()
        });
    };


    render() {
        const parentProps = {
            setAuth: this.setAuth,
            auth: this.state.auth,
            role: this.state.role,
            name: this.state.name,
            nationalName: this.state.nationalName,
            lastName: this.state.lastName,
            shaba: this.state.shaba,
            id: this.state.id,
            // new params from extra branch
            college: this.state.college,
            semester: this.state.semester,
            setSemester: sem => {
                this.setState({
                    semester: sem
                })
            },
            setCollege: coll => {
                this.setState({
                    college: coll
                })
            },
            dynamicInit : data=>{
                this.setState({
                    semester: data.semester,
                    college: data.college
                })
            }
        };

        return (
            <div>
                {/*<Route*/}
                {/*    path="/login"*/}
                {/*    render={params => {*/}
                {/*        return <Login params={params} parentProps={parentProps}/>;*/}
                {/*    }}*/}
                {/*/>*/}
                <Route
                    exact
                    path="/home"
                    render={params => {
                        return <Home params={params} parentProps={parentProps}/>;
                    }}
                />
                <Route
                    path="/sign-in"
                    render={params => {
                        return <SignIn params={params} parentProps={parentProps}/>;
                    }}
                />
                {!this.state.role && <Redirect to="/sign-in"/>}
                {this.state.role && <Home parentProps={parentProps}/>}
            </div>
        );
    }
}

export default Main;
