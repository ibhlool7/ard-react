import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import {BrowserRouter} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import {ThemeProvider} from '@material-ui/core/styles';
import {theme} from './constants/Theme'


ReactDOM.render(
    <ThemeProvider theme={theme}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </ThemeProvider>
    ,
    document.getElementById("root")
);
