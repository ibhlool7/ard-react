export const cook = "ard-app-cook";
export const api = "http://127.0.0.1:8000/";
export const userDomainPath = "api/v1/user/"
export const reportDomainPath = "api/v1/report/"

export function header(auth) {
    if (auth === null) {
        return {
            header: {
                "Content-Type": "application/json"
            }
        };
    } else {
        return {
            header: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${auth}`
            }
        };
    }
}

export const errors = {
    404: "user not found",
    406: "your user has been created before!",
    0: "خطایی در سیستم روی داده است لطفا مجددا تلاش نمایید"
};
export const USER = "user";
export const ADMIN = "admin";
export const HALF_YEARS = ["98-99-1", "98-99-2", "98-99-3", "99-00-1", "99-00-2", "99-00-3", "00-01-1", "00-01-2", "00-01-3",
    "01-02-1", "01-02-2", "01-02-3", "02-03-1", "02-03-2", "02-03-3", "03-04-1", "03-04-2", "03-04-3"]

export function cooking(auth, role, name, nationalName, lastName, id, shaba) {
    return {
        auth: auth,
        role: role,
        name: name,
        nationalName: nationalName,
        lastName: lastName,
        id: id,
        shaba : shaba
    };
}

export const AddUserComponent = "addUser";
export const AddSuperUserComponent = "addSuperUser";
export const AddUserInvoiceComponent = "addUserInvoice";
export const UserListComponent = "UserListComponent";
export const InvoiceListComponent = "UserListComponent";
export const ValueManagement = "ValueManagement";
export const PrintComp = "PrintComp";


export const FormUser = "FormUser";
export const UserTable = "UserATable";


export function getPureValueFromFormatNumberComponent(formatedValue) {
    const splited = formatedValue.split('')
    const reg = /^\d+$/;
    let pureValue = "";
    splited.forEach(item => {
        if (item.match(reg)) {
            pureValue += item
        }
    })

    return pureValue;
}

// --- convent values ---
export const FieldsNames = [
    {
        name: "فنی",
        value: "f"
    }, {
        name: "کشاورزی",
        value: "k"
    }, {
        name: "انسانی",
        value: "a"
    }, {
        name: "دام پزشکی",
        value: "d"
    }, {
        name: "مجازی",
        value: "m"
    }
]
export const Terms = ["98-99-1", "98-99-2", "98-99-3", "99-00-1", "99-00-2", "99-00-3", "00-01-1", "00-01-2", "00-01-3",
    "01-02-1", "01-02-2", "01-02-3", "02-03-1", "02-03-2", "02-03-3", "03-04-1", "03-04-2", "03-04-3"]

// --- convent values ---